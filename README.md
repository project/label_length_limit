## INTRODUCTION

Label fields have a limitation, sometimes it needs to exceed this limitation,
this module gives this functionality to extend the label 
limit to max 255 characters.

Usage:
after install,go to admin/config/label_length_limit/labellengthlimit 
and set the max value limit.its default value is 150

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/label_length_limit

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/label_length_limit


## REQUIREMENTS

No special requirements.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

No configuration is needed.


## MAINTAINERS

Current maintainers:
 * Yusef Mohamadi (yuseferi) - https://www.drupal.org/u/yuseferi
